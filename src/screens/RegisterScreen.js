import React, { useState } from 'react';
import { View, ScrollView, Image, StyleSheet, ImageBackground } from 'react-native';
import { Button, TextInput, HelperText } from "react-native-paper";
import Logo from '../../assets/images/loundy_logo-removebg-preview.png';
import BackgroundHome from '../../assets/images/home-bg.png';
import Colors from '../../constant/Colors';
import { requestRegister } from '../api/AuthAPI';

const RegisterScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [register, setRegister] = useState({
        email: '',
        password: '',
        phone: '',
        emailError: false,
        passwordError: false,
        confirmationError: false,
        phoneError: false,
        submit: false,
        secure: true
    });

    const handleInputEmail = (email) => {
        if (email.length > 2 && email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/g)) {
            setRegister({
                ...register,
                email,
                emailError: false,
                submit: true
            });
        } else {
            setRegister({
                ...register,
                email,
                emailError: true,
                submit: false
            });
        }
    }

    const handleInputPassword = (password) => {
        if (password.length >= 8 && password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)) {
            setRegister({
                ...register,
                password,
                passwordError: false,
                submit: true
            });
        } else {
            setRegister({
                ...register,
                password,
                passwordError: true,
                submit: false
            });
        }
    }

    const handleInputPhone = (phone) => {
        if (phone.length > 5 && phone.match(/^[0][0-9]{5,15}$/g)) {
            setRegister({
                ...register,
                phone,
                phoneError: false,
                submit: true
            });
        } else {
            setRegister({
                ...register,
                phone,
                phoneError: true,
                submit: false
            });
        }
    }

    const handlePasswordConfirmation = (passwordConfirmation) => {
        if (passwordConfirmation === register.password) {
            setRegister({
                ...register,
                confirmationError: false,
                submit: true
            });
        } else {
            setRegister({
                ...register,
                confirmationError: true,
                submit: false
            });
        }
    }

    const submitRegister = () => {
        setLoading(true);
        requestRegister({
            email: register.email,
            password: register.password,
            phone: register.phone
        }).then(response => {
            console.log(response);
            setLoading(false);
            navigation.navigate('SuccessRegister')
        }).catch(err => {
            setLoading(false);
            alert("Email already used");
        })
    }

    return (
        <ImageBackground
            source={BackgroundHome}
            style={{
                height: '100%',
                width: '100%'
            }}>
            <ScrollView style={styles.root}>
                <View style={styles.imageBox}>
                    <Image source={Logo} style={styles.logo} resizeMode="contain" />
                </View>
                <TextInput
                    onChangeText={handleInputEmail}
                    mode="outlined"
                    label="Email"
                    placeholder="Input email"
                    style={styles.input}
                />
                <HelperText type="error" visible={register.emailError}>
                    Email address is invalid!
                </HelperText>

                <TextInput
                    mode="outlined"
                    onChangeText={handleInputPassword}
                    secureTextEntry={register.secure}
                    label="Password"
                    placeholder="Input password"
                    style={styles.input}
                    right={<TextInput.Icon name="eye"
                        onPress={() => setRegister({ ...register, secure: !register.secure })} />}
                />
                <HelperText type="error" visible={register.passwordError}>
                    Password min 8 character and least one Uppercase letter and number
                </HelperText>

                <TextInput
                    mode="outlined"
                    onChangeText={handlePasswordConfirmation}
                    secureTextEntry={register.secure}
                    label="Password Confirmation"
                    placeholder="Input password confirmation"
                    style={styles.input}
                    right={<TextInput.Icon name="eye"
                        onPress={() => setRegister({ ...register, secure: !register.secure })} />}
                />
                <HelperText type="error" visible={register.confirmationError}>
                    Password not match
                </HelperText>

                <TextInput
                    mode="outlined"
                    onChangeText={handleInputPhone}
                    label="Phone Number"
                    placeholder="Input phone number"
                    style={styles.input}
                />
                <HelperText type="error" visible={register.phoneError}>
                    Number only, and start with '0'
                </HelperText>

                <Button
                    mode="contained"
                    style={styles.button}
                    onPress={submitRegister}
                    disabled={!register.submit}
                    loading={loading}
                >Submit</Button>
                <Button
                    icon="login"
                    mode="contained"
                    color={Colors.light}
                    style={styles.buttonSignin}
                    onPress={() => navigation.navigate('SignIn')}
                >Sign In</Button>
            </ScrollView>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        padding: 20
    },
    imageBox: {
        justifyContent: 'center',
        flexDirection: "row"
    },
    logo: {
        width: '70%',
        maxWidth: 320,
        maxHeight: 200
    },
    input: {
        paddingVertical: 0,
        height: 50
    },
    button: {
        marginTop: 20,
        paddingVertical: 3,
    },
    buttonSignin: {
        marginTop: 20,
        marginBottom: 50
    }
});

export default RegisterScreen;
