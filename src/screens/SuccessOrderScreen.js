import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';
import Successfully from '../../assets/images/winner-people-rev.png'
import Colors from '../../constant/Colors';

const SuccessOrderScreen = ({ navigation }) => {
    return (
        <View style={styles.root}>
            <Image
                style={styles.image}
                source={Successfully}
            />
            <Text style={styles.title}>Horay, Success Checkout</Text>
            <Button
                icon="home"
                mode="outlined"
                color={Colors.primary}
                style={styles.button}
                onPress={() => navigation.navigate('Home')}
            >Home</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.light
    },
    image: {
        width: 400,
        height: 280
    },
    title: {
        fontSize: 28,
        color: Colors.primary
    },
    button: {

        marginTop: 20,
        paddingVertical: 3
    }
});


export default SuccessOrderScreen;