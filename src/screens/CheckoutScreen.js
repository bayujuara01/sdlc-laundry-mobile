import React from 'react'
import { ScrollView, ImageBackground, StyleSheet } from 'react-native'
import { View } from 'react-native-animatable';
import { Button, Card, FAB, Text } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Background from '../../assets/images/home-bg.png';
import Colors from '../../constant/Colors';
import { requestOrderServices } from '../api/ServiceAPI';
import { serviceActions } from '../store/servicesSlice';


const CheckoutScreen = ({ navigation }) => {
    const { service, auth } = useSelector(state => state);
    const dispatch = useDispatch();

    console.log(service);

    const submitCheckout = () => {
        requestOrderServices({
            partnerId: service.partnerId,
            data: service.selected.map(sv => ({
                customerId: auth.user.id,
                serviceId: sv.id,
                qty: sv.qty
            }))
        }).then(response => {
            console.log(response);
            navigation.navigate("Order")
            dispatch(serviceActions.resetSelected());
        }).catch(err => {
            alert("Internal Server Error");
        })
    }

    const Cart = () => service.selected.map(sv => {
        return (
            <Card key={sv.id} style={styles.serviceCard}>
                <Card.Title
                    title={sv.name}
                    subtitle={`Price : IDR ${sv.price}`}

                />
                <Card.Content style={styles.serviceContainer}>
                    <View>
                        <Text>Amount : {sv.qty}</Text>
                    </View>

                    <Text style={styles.textTotal}>IDR {sv.price * sv.qty}</Text>
                </Card.Content>
            </Card>
        );
    });


    return (
        <ImageBackground
            source={Background}
            style={styles.root}>

            <ScrollView style={styles.container}>
                <Text>Checkout</Text>
                <Card>
                    <Text style={styles.title}>Detail Transaction</Text>
                </Card>
                <Cart />
                <Card>
                    <Text style={styles.title}>IDR {service.total}</Text>
                </Card>


            </ScrollView>
            <FAB
                style={styles.fab}
                label="Order"
                icon="check"
                color="white"
                onPress={submitCheckout} />
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        width: "100%",
        height: "100%",
    },
    container: {
        paddingTop: 10,
        paddingHorizontal: 16
    },
    title: {
        fontSize: 26,
        padding: 8,
        textAlign: 'center',

    },
    serviceCard: {
        marginVertical: 8
    },
    serviceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    serviceName: {
        fontSize: 24,
    },
    serviceQty: {
        fontSize: 14
    },
    servicePrice: {
        fontSize: 14
    },
    textTotal: {
        fontSize: 16
    },
    fab: {
        position: 'absolute',
        backgroundColor: Colors.primary,
        color: Colors.light,
        margin: 16,
        right: 0,
        bottom: 0,
    }
});

export default CheckoutScreen;
