import React, { useCallback, useEffect, useState } from 'react';
import { View, StyleSheet, ImageBackground, Image, Text, ScrollView } from 'react-native';
import { Avatar, Button, IconButton, FAB, Card } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Background from '../../assets/images/account-bg.png';
import Colors from '../../constant/Colors';
import Counter from 'react-native-counters';
import { getServices } from '../api/ServiceAPI';
import { serviceActions } from '../store/servicesSlice';

const DetailLaundryScreen = ({ route, navigation }) => {
    const dispatch = useDispatch();
    const { partner } = route.params
    const { laundry, service } = useSelector(state => state);
    const [choice, setChoice] = useState({
        partnerId: null,
        services: {}
    });

    useEffect(() => {
        fetchLaundryService();
    }, []);

    const fetchLaundryService = useCallback(() => {
        getServices().then(response => {
            dispatch(serviceActions.setServices({ services: response.data.data }));
            // console.log(response.data);
        }).catch((err) => {
            alert("Internal Server Error, please try again");
        })
    }, []);

    const onInputChange = (data, input) => {
        setChoice({
            ...choice,
            partnerId: data.partnerId,
            services: {
                ...choice.services,
                [data.serviceId]: {
                    id: data.serviceId,
                    qty: input,
                    price: data.price,
                    name: data.name
                }
            }
        })
    }

    const onSubmitCheckout = () => {
        const choiceServices = []
        for (let key in choice.services) {
            if (choice.services[key].qty > 0) {
                choiceServices.push(choice.services[key]);
            }
        }
        dispatch(serviceActions.setSelected({ services: choiceServices, partnerId: choice.partnerId }));
        navigation.navigate('Checkout');
    }

    return (
        <ImageBackground
            source={Background}
            style={styles.root}>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-start'
            }}>
                <IconButton
                    icon="arrow-left"
                    size={36}
                    color={Colors.white}
                    onPress={() => navigation.navigate('Home')} />
            </View>
            <ScrollView style={styles.container}>

                <View style={styles.card}>
                    <Image source={{ uri: "https://picsum.photos/200" }} style={styles.avatar} />
                    <Text style={styles.textName}>{partner.username}</Text>
                    <Text style={styles.textEmail}>{partner.distance.toFixed(2)} Km</Text>
                </View>

                {service.services.map((service, index) => {
                    return (
                        <Card key={index} style={styles.servicesCard} >
                            <Card.Title
                                subtitle={service.name}
                                title={'Rp. ' + service.price}
                                left={(props) => <Avatar.Icon {...props} icon="washing-machine" />}
                                right={(props) => <Counter start={0}
                                    onChange={(input) => onInputChange({
                                        partnerId: partner.id,
                                        serviceId: service.id,
                                        price: service.price,
                                        name: service.name
                                    }, input)} />}
                            />
                        </Card>
                    );
                })}
            </ScrollView>
            <FAB
                style={styles.fab}
                label="Checkout"
                icon="check"
                color="white"
                onPress={() => onSubmitCheckout()}
            />
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        width: "100%",
        height: "100%",
        paddingTop: 10
    },
    container: {
        marginTop: 20,
        paddingHorizontal: 20
    },
    card: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 70
    },
    avatar: {
        height: 120,
        width: 120,
        borderRadius: 150,
        borderWidth: 5,
        borderColor: 'white'
    },
    btnLogout: {
        color: 'white',
    },
    textName: {
        fontSize: 28,
        marginTop: 20,
        color: Colors.primaryDark,
        backgroundColor: Colors.white,
        paddingHorizontal: 10,
        borderRadius: 50
    },
    textEmail: {
        fontSize: 18,
        color: Colors.primaryDark,
        backgroundColor: Colors.white,
        paddingHorizontal: 10,
        paddingBottom: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    servicesCard: {
        marginVertical: 7,
        paddingRight: 15
    },
    services: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.white,
        padding: 10,
        borderRadius: 5,

    },
    servicesTitle: {
        fontSize: 16
    }, fab: {
        position: 'absolute',
        backgroundColor: Colors.primary,
        color: Colors.light,
        margin: 16,
        right: 0,
        bottom: 0,
    }
});

export default DetailLaundryScreen;
