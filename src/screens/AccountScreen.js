import React, { useCallback, useEffect, useState } from 'react'
import { View, StyleSheet, ImageBackground, Image, Text } from 'react-native'
import { Avatar, IconButton, Card, Button } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import BackgroundAccount from '../../assets/images/account-bg.png';
import { useDispatch, useSelector } from 'react-redux';
import { authActions } from '../store/authSlice';
import Colors from '../../constant/Colors';
import { requestOrderTransactionHistory } from '../api/ServiceAPI';
import { useFocusEffect } from '@react-navigation/core';

const AccountScreen = ({ navigation }) => {
    const { auth } = useSelector(state => state);
    const [transactionHistory, setTransactionHistory] = useState([]);
    const dispatch = useDispatch();

    useFocusEffect(
        useCallback(() => {
            requestOrderTransactionHistory({ customerId: auth.user.id })
                .then(response => {
                    setTransactionHistory([...response.data.data]);
                }).catch(err => {
                    alert("Internal Server Error")
                });
        }, [])
    );

    const handleLogoutButton = () => {
        dispatch(authActions.logout());
    }

    return (
        <ImageBackground
            source={BackgroundAccount}
            style={styles.root}>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end'
            }}>
                <IconButton
                    icon="logout"
                    size={36}
                    color={Colors.white}
                    onPress={handleLogoutButton} />
            </View>
            <ScrollView style={styles.container}>

                <View style={styles.card}>
                    <Image source={{ uri: "https://picsum.photos/200" }} style={styles.avatar} />
                    <Text style={styles.textName}>Bayu Seno Ariefyanto</Text>
                    {/* <Text style={styles.textEmail}>ariefyantobayu@gmail.com</Text> */}
                </View>

                <View>
                    <Card style={styles.titleBox}>
                        <Text style={styles.title}>Order History</Text>
                    </Card>


                    <ScrollView>
                        {transactionHistory.map((trx, index) => {
                            return (
                                <Card key={trx.id} style={styles.historyBox} onPress={() => navigation.navigate('DetailOrder', { transaction: trx })}>
                                    <Card.Title
                                        title={trx.partnerName}
                                        subtitle={`Rp. ${trx.grandTotal}`}
                                        right={(props) => <Button {...props}>{trx.statusName}</Button>}
                                    />
                                </Card>
                            );
                        })}
                    </ScrollView>
                </View>
            </ScrollView>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        width: "100%",
        height: "100%",
        paddingTop: 10,
    },
    container: {
        marginTop: 20,
        paddingHorizontal: 20,
    },
    card: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        height: 120,
        width: 120,
        borderRadius: 150,
        borderWidth: 5,
        borderColor: 'white'
    },
    btnLogout: {
        color: 'white',
    },
    textName: {
        fontSize: 28,
        marginTop: 20,
        color: Colors.primaryDark,
        backgroundColor: Colors.white,
        paddingHorizontal: 10,
        borderRadius: 50
    },
    textEmail: {
        fontSize: 18,
        color: Colors.primaryDark,
        backgroundColor: Colors.white,
        paddingHorizontal: 10,
        paddingBottom: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    titleBox: {
        paddingVertical: 5,
        marginTop: 40,
        marginBottom: 20
    },
    title: {
        fontSize: 24,
        textAlign: 'center'
    },
    historyBox: {
        marginVertical: 5
    }
});

export default AccountScreen;
