import React, { useEffect, useState } from 'react'
import { View, Text, ImageBackground, StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Card, FAB } from 'react-native-paper';
import { useSelector } from 'react-redux';
import Background from '../../assets/images/home-bg.png';
import Colors from '../../constant/Colors';
import {
    requestOrderTransactionDetail,
    requestMakePaidTransaction
} from '../api/ServiceAPI';

const DetailTransactionScreen = ({ navigation, route }) => {
    const { auth } = useSelector(state => state);
    const { transaction } = route.params;
    const [detailTransaction, setDetailTransaction] = useState([])
    const [update, setUpdate] = useState(false);

    useEffect(() => {
        async function getDetailTransaction() {
            requestOrderTransactionDetail({
                customerId: auth.user.id,
                transactionId: transaction.id
            }).then(response => {
                console.log(response.data.data.details)
                setDetailTransaction([...response.data.data.details]);
            }).catch(err => {
                alert("Internal Server Error");
            })
        }

        getDetailTransaction();
    }, [update]);

    const makePaidTransaction = ({ transactionId }) => {
        requestMakePaidTransaction({ transactionId })
            .then(response => {
                alert('Transaction has been paid');
                setUpdate(!update);
                navigation.navigate('Account');
            }).catch(err => {
                alert('Internal Server Error');
            })
    }

    return (
        <ImageBackground
            source={Background}
            style={styles.root}>
            <View style={styles.container}>
                <Card style={styles.titleBox}>
                    <Text style={styles.titleText}>Detail transaction</Text>
                    <Card.Content>
                        <View>
                            <Text style={styles.label}>Partner</Text>
                            <Text style={styles.text}>{transaction.partnerName}</Text>
                            <Text style={styles.label}>Grand Total</Text>
                            <Text style={styles.text}>IDR {transaction.grandTotal}</Text>
                            <Text style={styles.label}>Generated</Text>
                            <Text style={styles.text}>{new Date(transaction.createAt).toLocaleString()}</Text>
                        </View>
                    </Card.Content>
                </Card>
                <ScrollView>
                    <View style={styles.serviceBox}>
                        {detailTransaction.map(detail => {
                            return <Card style={styles.serviceCard}>
                                <Card.Title
                                    title={detail.serviceName}
                                    subtitle={`Price : IDR ${detail.price}`}

                                />
                                <Card.Content style={styles.serviceContainer}>
                                    <View>
                                        <Text>Amount : {detail.qty}</Text>
                                    </View>

                                    <Text style={styles.textTotal}>IDR {detail.price * detail.qty}</Text>
                                </Card.Content>
                            </Card>
                        })}

                    </View>
                </ScrollView>
            </View>
            {transaction.statusName == 'PROCESS' && <FAB
                style={styles.fab}
                label="Paid"
                color="white"
                onPress={() => makePaidTransaction({ transactionId: transaction.id })}
            />}
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        width: "100%",
        height: "100%",
    },
    container: {
        marginTop: 16
    },
    titleBox: {
        padding: 10,
        marginTop: 5,
        marginHorizontal: 10
    },
    titleText: {
        fontSize: 24,
        textAlign: 'center'
    },
    serviceBox: {
        paddingHorizontal: 10
    },
    serviceCard: {
        marginVertical: 8
    },
    serviceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    serviceName: {
        fontSize: 24,
    },
    serviceQty: {
        fontSize: 14
    },
    servicePrice: {
        fontSize: 14
    },
    textTotal: {
        fontSize: 16
    },
    label: {
        fontSize: 12,
        color: Colors.darkGray
    },
    text: {
        fontSize: 16
    },
    fab: {
        position: 'absolute',
        backgroundColor: Colors.primary,
        color: Colors.light,
        margin: 16,
        right: 0,
        bottom: 0,
    }
});

export default DetailTransactionScreen;
