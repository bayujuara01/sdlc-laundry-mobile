import React, { useState } from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    useWindowDimensions,
    ImageBackground
} from "react-native";
import Logo from '../../assets/images/loundy_logo-removebg-preview.png';
import Background from '../../assets/images/account-bg-rev.png';
import CustomInput from "../components/CustomInput";
import { Button, TextInput, HelperText } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../store/authSlice";

import axios from "axios";
import jwtDecode from "jwt-decode";
import { requestLogin } from "../api/AuthAPI";
import Colors from "../../constant/Colors";

const SignInScreen = ({ navigation }) => {
    const auth = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [login, setLogin] = useState({
        email: '',
        password: '',
        emailError: false,
        passwordError: false,
        submit: false,
        secure: true
    });

    const handleInputEmail = (email) => {
        if (email.length > 2 && email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/g)) {
            setLogin({
                ...login,
                email,
                emailError: false,
                submit: false
            });
        } else {
            setLogin({
                ...login,
                email,
                emailError: true,
                submit: true
            });
        }
    }

    const handleInputPassword = (password) => {
        if (password.length >= 8 && password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)) {
            setLogin({
                ...login,
                password,
                passwordError: false,
                submit: false
            });
        } else {
            setLogin({
                ...login,
                password,
                passwordError: true,
                submit: true
            });
        }
    }

    const submitLogin = () => {
        dispatch(authActions.initializeLogin());
        requestLogin({
            email: login.email,
            password: login.password
        }).then(response => {
            const token = response.headers['authorization'].substring(7);
            const { roles, sub } = jwtDecode(token);

            if (roles.includes("USER")) {
                // console.log(token);
                dispatch(authActions.login({ token, role: roles[0], id: sub }));
            }
        }).catch(err => {
            dispatch(authActions.failedLogin());
            alert("Wrong on username or password")
            console.log(err);
        });
    }

    const { height } = useWindowDimensions();

    return (
        <ImageBackground
            source={Background}
            style={{
                height: '100%',
                width: '100%',
            }}>
            <View style={styles.root}>
                <View style={styles.imageBox}>
                    <Image source={Logo} style={styles.logo} resizeMode="contain" />
                </View>
                <View style={styles.inputBox}>
                    <TextInput
                        onChangeText={handleInputEmail}
                        mode="outlined"
                        label="Email"
                        placeholder="Input email"
                        style={styles.input}
                    />
                    <HelperText type="error" visible={login.emailError}>
                        Email address is invalid!
                    </HelperText>
                </View>

                <TextInput
                    mode="outlined"
                    onChangeText={handleInputPassword}
                    secureTextEntry={login.secure}
                    label="Password"
                    placeholder="Input password"
                    style={styles.input}
                    right={<TextInput.Icon name="eye" onPress={() => setLogin({ ...login, secure: !login.secure })} />}
                />
                <HelperText type="error" visible={login.passwordError}>
                    Password min 8 character and least one Uppercase letter and number
                </HelperText>

                <View style={styles.buttonBox}>
                    <Button
                        disabled={login.submit}
                        icon="login"
                        mode="contained"
                        style={styles.button}
                        onPress={submitLogin}
                        loading={auth.isLoading}
                    >Sign In</Button>
                    <Button
                        icon="account-plus"
                        color={Colors.light}
                        mode="contained"
                        style={styles.button}
                        onPress={() => navigation.navigate("Register")}
                    >Register</Button>
                </View>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    root: {
        marginTop: 40,
        padding: 20,
    },
    imageBox: {
        justifyContent: 'center',
        flexDirection: "row"
    },
    logo: {
        width: '70%',
        maxWidth: 320,
        maxHeight: 200
    },
    inputBox: {
        paddingVertical: 10
    },
    input: {

    },
    buttonBox: {
        marginTop: 80
    },
    button: {
        marginTop: 20,
        paddingVertical: 3
    }
});

export default SignInScreen;