import React, { useCallback, useEffect, useState } from 'react'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { View, Text, StyleSheet, ImageBackground, ScrollView, Image, } from 'react-native'
import { Card, BottomNavigation, Avatar } from 'react-native-paper';
import BackgroundHome from '../../assets/images/home-bg.png';
import CustomCard from '../components/molecules/CustomCard';
import { getLaundryById, getNearbyLaundry } from '../api/LaundryAPI';
import { useDispatch, useSelector } from 'react-redux';
import { locationActions } from '../store/locationSlice';
import * as Location from 'expo-location';
import { serviceActions } from '../store/servicesSlice';
import { laundryActions } from '../store/laundrySlice';
import { useFocusEffect } from '@react-navigation/core';
import { getServices } from '../api/ServiceAPI';

const HomeScreen = ({ navigation }) => {
    const { location, laundry, service } = useSelector(state => state);
    const dispatch = useDispatch();

    useFocusEffect(
        useCallback(() => {
            getCurrentLocation();
        }, [])
    );

    useEffect(() => {
        fetchNerbyLaundry({
            latitude: location.coord.latitude,
            longitude: location.coord.longitude
        });
        fetchLaundryService();
    }, [location]);

    const getCurrentLocation = async () => {
        console.log("FOCUS TO HOME");
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
        }

        let location = await Location.getCurrentPositionAsync();
        dispatch(locationActions.setLocation({ coord: location.coords }));
    }

    const fetchNerbyLaundry = ({ latitude, longitude }) => {
        getNearbyLaundry({
            latitude,
            longitude
        }).then(response => {
            console.log("FETCHING LAUNDRY");
            dispatch(laundryActions.setLaundry(response.data));
        }).catch(err => {
            alert("Internal Server Error");
        });
    };

    const fetchLaundryService = useCallback(() => {
        getServices().then(response => {
            dispatch(serviceActions.setServices({ services: response.data.data }));
            // console.log(response.data);
        }).catch((err) => {
            alert("Internal Server Error, please try again");
        })
    }, []);

    return (
        <ImageBackground
            source={BackgroundHome}
            style={{
                width: "100%",
                height: "100%",

            }}>
            <View style={{
                height: '88%'
            }}>
                <ScrollView>
                    <View style={{
                        // backgroundColor: 'red',
                        // width: '100%',
                        height: 40
                    }}>

                    </View>
                    <Card style={{
                        marginTop: 10,
                        marginHorizontal: 10
                    }}>
                        <Card.Title
                            title="Nearest Laundry" />
                    </Card>

                    {laundry.laundries.length > 0 ? <>
                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            style={{ marginHorizontal: 10, marginTop: 10 }}
                        >
                            {laundry.laundries.map((ldy, index) => {
                                return (
                                    <CustomCard iconed key={index}
                                        text={ldy.username}
                                        imageSrc="https://picsum.photos/200"
                                        onPress={() => navigation.navigate('DetailLaundry', { partner: ldy })} />
                                );
                            })}

                        </ScrollView>
                    </> : <>
                        <Card style={{ marginVertical: 5, }}>
                            <Card.Title
                                title="No Nearby Laundries"
                                subtitle="We will coming soon" />
                        </Card>
                    </>}

                    <Card style={{
                        marginTop: 20,
                        marginHorizontal: 10
                    }}>
                        <Card.Title
                            title="Our Services"
                            left={(props) => <Avatar.Icon {...props} icon="washing-machine" />} />
                    </Card>
                    <View style={{ flex: 1, height: 400 }}>
                        <ScrollView nestedScrollEnabled={true} style={{
                            flexGrow: 0.10,
                            marginTop: 10,
                            marginHorizontal: 10
                        }}>
                            {service.services.map((value, index) => (
                                <Card
                                    key={index}
                                    onPress={() => navigation.navigate('Laundry')}
                                    style={{ marginVertical: 5, }}>
                                    <Card.Title title={value.name} subtitle={`Rp. ${value.price}`} />
                                </Card>
                            ))}
                        </ScrollView>
                        <View style={{ flex: 0.90 }} />
                    </View>
                </ScrollView>
            </View>
        </ImageBackground >
    );
}


export default HomeScreen;
