import React from 'react'
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    ImageBackground,
    Image,
    Dimensions
} from 'react-native'
import { Card, Avatar } from 'react-native-paper';
import { useSelector } from 'react-redux';
import Background from '../../assets/images/home-bg.png';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../constant/Colors';

const { width: screenWidth } = Dimensions.get('window')

const LaundryScreen = ({ navigation }) => {
    const laundry = useSelector(state => state.laundry);

    const renderBanners = null;

    return (
        <ImageBackground
            source={Background}
            style={{ width: "100%", height: "100%" }}
        >
            <View>
            </View>
            <ScrollView nestedScrollEnabled={true} style={{
                flexGrow: 0.30,
                marginTop: 20,
                marginHorizontal: 10
            }}>
                {laundry.laundries.map((value) => (
                    <Card
                        key={value.id}
                        style={{ marginVertical: 5, }}
                        onPress={() => navigation.navigate('DetailLaundry', { partner: value })}>
                        <Card.Content style={{ height: 160, flex: 1, flexDirection: 'row' }}>
                            <Image source={{
                                uri: "https://picsum.photos/200"
                            }} style={styles.leftImage} />
                            <View>
                                <Text style={styles.title}>{value.username}</Text>
                                <View style={styles.marker}>
                                    <Icon name="map-marker" size={25} color={Colors.primary} />
                                    <Text style={styles.distance}>{value.distance.toFixed(2)} Km</Text>
                                </View>

                            </View>
                        </Card.Content>
                    </Card>
                ))}
            </ScrollView>
            {/* <View style={{ flex: 0.90, backgroundColor: 'green' }} /> */}
        </ImageBackground >
    )
}


const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        paddingHorizontal: 20
    },
    title: {
        fontSize: 28,
        width: 200
    },
    distance: {
        fontSize: 14,
    },
    leftImage: {
        height: '100%',
        width: 140,
        borderRadius: 8,
        marginRight: 20
    },
    marker: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default LaundryScreen;
