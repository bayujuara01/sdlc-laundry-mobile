import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import {
    persistStore,
    persistReducer,
    FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER
} from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import authReducer from './authSlice';
import locationReduer from './locationSlice';
import laundryReducer from './laundrySlice';
import servicesReducer from "./servicesSlice";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['location']
}

const rootReducer = combineReducers({
    auth: authReducer,
    location: locationReduer,
    laundry: laundryReducer,
    service: servicesReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
        }
    })
});

export const persistor = persistStore(store);



