import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    coord: {
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        latitude: 0,
        longitude: 0,
        speed: 0
    }
}

const setLocation = (state, { payload }) => {
    // console.log(payload);
    state.coord = payload.coord;
}

const locationSlice = createSlice({
    name: 'location',
    initialState,
    reducers: {
        setLocation
    }
});

export const locationActions = locationSlice.actions;

export default locationSlice.reducer;