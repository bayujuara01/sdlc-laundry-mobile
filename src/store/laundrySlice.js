import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    laundries: [],
    selected: {
        id: null,
        name: '',
        coord: {
            latitude: 0.0,
            longitude: 0.0
        },
        distance: 0.0,
        imageUrl: ''
    }
}

const setLaundry = (state, { payload }) => {
    console.log(payload.data);
    state.laundries = payload.data;
}

const setDetailLaundry = (state, { payload }) => {
    console.log(payload);
}

const laundrySlice = createSlice({
    name: 'laundry',
    initialState,
    reducers: {
        setLaundry,
        setDetailLaundry
    }
});

export const laundryActions = laundrySlice.actions;

export default laundrySlice.reducer;