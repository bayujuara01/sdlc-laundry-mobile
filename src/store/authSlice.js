import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    isAuth: false,
    isLoading: false,
    isError: false,
    user: {
        id: '',
        role: '',
        email: '',
        password: ''
    },
    token: ''
}

const initializeLogin = (state) => {
    state.isLoading = true;
    state.isError = false;
}

const login = (state, { payload }) => {
    state.isLoading = false;
    state.isError = false;
    state.isAuth = true;
    state.token = payload.token;
    state.user = {
        id: payload.id,
        role: payload.role
    }
}

const failedLogin = (state) => {
    state.isLoading = false;
    state.isError = true;
}

const logout = (state) => {
    state.isAuth = false;
    state.isLoading = false;
    state.isError = false;
    state.token = '';
    state.user = {
        id: '',
        role: '',
        email: '',
        password: ''
    }
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login,
        logout,
        initializeLogin,
        failedLogin
    }
});

export const authActions = authSlice.actions;

export default authSlice.reducer;