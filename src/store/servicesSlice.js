import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    services: [],
    selected: [],
    total: 0,
    partnerId: null
}

const setServices = (state, { payload }) => {
    state.services = payload.services;
}

const setSelected = (state, { payload }) => {
    state.selected = [...payload.services];
    state.total = payload.services.reduce((accumulator, next) => {
        return accumulator + (next.price * next.qty);
    }, 0);
    state.partnerId = payload.partnerId;
}

const resetSelected = (state) => {
    state.selected = [];
    state.total = 0;
    state.partnerId = null;
}

const serviceSlice = createSlice({
    name: 'service',
    initialState,
    reducers: {
        setServices,
        setSelected,
        resetSelected
    }
});

export const serviceActions = serviceSlice.actions;

export default serviceSlice.reducer;