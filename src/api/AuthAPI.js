import Api from "./Api";

export const requestLogin = ({ email, password }) => Api.post('/authenticate', { email, password });

export const requestRegister = ({ email, password, phone }) => {
    return Api.post('/customer/register', { email, password });
}