import Api from "./Api";

export const getServices = () => Api.get('/services');

export const requestOrderServices = ({ partnerId, data }) => {
    return Api.post(`/partner/${partnerId}/transaction`, data);
}

export const requestOrderTransactionHistory = ({ customerId }) => {
    return Api.get(`/customer/${customerId}/transaction`);
}

export const requestOrderTransactionDetail = ({ customerId, transactionId }) => {
    return Api.get(`/customer/${customerId}/transaction/${transactionId}`)
}

export const requestMakePaidTransaction = ({ transactionId }) => {
    return Api.post(`/customer/transaction/${transactionId}/paid`);
}