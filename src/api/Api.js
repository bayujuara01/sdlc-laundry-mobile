import axios from "axios";
import { SERVER_URL } from "../../constant/Config";

let store;

const Api = axios.create({
    baseURL: SERVER_URL,
    headers: {
        'Content-Type': 'application/json'
    },
    timeout: 5000
});

Api.interceptors.request.use(config => {
    config.headers.authorization = 'Bearer ' + store.getState().auth.token;
    return config;
})

export const injectStore = _store => {
    store = _store;
}

export default Api;

