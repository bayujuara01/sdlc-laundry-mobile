import Api from "./Api";

export const getLaundryById = (id) => Api.get('/hello');

export const getNearbyLaundry = ({ latitude, longitude }) => {
    return Api.get(`/partners?lat=${latitude}&lng=${longitude}`);
}

