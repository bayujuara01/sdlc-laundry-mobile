import React from "react";
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import SignInScreen from "../screens/SignInScreen";
import HomeScreen from "../screens/HomeScreen";
import TabNavigation from "./TabNavigation";
import { useSelector } from "react-redux";
import DetailLaundryScreen from "../screens/DetailLaundryScreen";
import RegisterScreen from "../screens/RegisterScreen";
import SuccessRegisterScreen from "../screens/SuccessRegisterScreen";
import CheckoutScreen from "../screens/CheckoutScreen";
import SuccessOrderScreen from "../screens/SuccessOrderScreen";
import DetailTransactionScreen from "../screens/DetailTransactionScreen";

const options = {
    gestureEnabled: true,
    gestureDirection: 'horizontal',
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    headerShown: false,
}

const Stack = createStackNavigator();


const RootNavigation = () => {
    const auth = useSelector(state => state.auth);
    return (
        <Stack.Navigator screenOptions={options}>
            {auth.isAuth ? (
                <>
                    <Stack.Screen name="Home" component={TabNavigation} />
                    <Stack.Screen
                        name="DetailLaundry"
                        component={DetailLaundryScreen}
                    />
                    <Stack.Screen name="Checkout" component={CheckoutScreen} />
                    <Stack.Screen name="Order" component={SuccessOrderScreen} />
                    <Stack.Screen name="DetailOrder" component={DetailTransactionScreen} />
                </>
            ) : (
                <>
                    <Stack.Screen name="SignIn" component={SignInScreen} />
                    <Stack.Screen name="Register" component={RegisterScreen} />
                    <Stack.Screen name="SuccessRegister" component={SuccessRegisterScreen} />
                </>
            )}
        </Stack.Navigator>
    );
}

export default RootNavigation;