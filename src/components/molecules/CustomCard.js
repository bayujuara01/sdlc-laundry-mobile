import React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Card } from "react-native-paper";


const CustomCard = ({
    imageSrc,
    iconed,
    onPress,
    icon = 'map-marker',
    text = 'Laundry Kuy Gas Keun' }) => {
    return (
        <Card
            style={styles.cardContainer}
            onPress={onPress}
        >
            <Image
                source={{
                    uri: imageSrc
                }}
                style={styles.image}
            />
            <View
                style={styles.content}
            >
                <View
                    style={{
                        paddingHorizontal: 5,
                        paddingVertical: 5,
                    }}
                >
                    <Text
                        style={styles.subtitle}
                    >
                        {text}
                    </Text>
                </View>
                {iconed && <Icon name={icon} size={25} color="#ff5c83" />}
            </View>
        </Card>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: "#FEFEFE",
        height: 200,
        width: 190,
        borderRadius: 15,
        padding: 5,
    },
    content: {
        flex: 1,
        flexDirection: "row",
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 180, borderRadius: 10, height: 130
    },
    subtitle: {
        fontSize: 14,
        color: "#a2a2db",
        textAlign: 'center'
    }
});

export default CustomCard;