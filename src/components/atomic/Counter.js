import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TextInput, Button } from 'react-native-paper';

const Counter = ({ counter = 0, setCounter }) => {

    return (
        <View style={{ flexDirection: 'row' }}>
            <Button mode="contained" style={styles.button} onPress={() => { }}>-</Button>
            <TextInput style={styles.input} value="0" />
            <Button mode="contained" style={styles.button} onPress={() => { }}>+</Button>
            <View style={styles.customButton} onPress>
                <Text>+</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    customButton: {
        width: 30,
        height: 30,
        backgroundColor: 'red'
    },
    button: {
        marginHorizontal: 0,
    },
    input: {
        width: 30,
        height: 30
    }
});

export default Counter;
